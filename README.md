# DQ test app

This app allows you to check if your datasets conform to a set of rules.
It is based on the [Great Expectations](https://github.com/great-expectations/great_expectations) python library.
When the component is run, a Snowflake workspace is created, the datasets specified in the input mapping are loaded in
and during the component run, the Python programme running in the Docker Container is connecting to this workspace and
the tests are performed in that workspace. All results are captured in the Docker Container and once all tests are
finished, the tables containing the results are loaded into the Snowflake workspace, where from they are loaded to 
Keboola Storage.

#### Output
The component outputs two tables:

- Performed Tests (tests that have been conducted)
    - table_name: name of the table on which the test has been conducted
    - test: name of the test
    - success: whether the dataset passed
    - config: the actual config of the test
    - result: the output of the test
    - timestamp: the timestamp when the test was performed
    
- Failed Tests (tests that have not been performed because of an issue)
    - table_name: name of the table on which the test has been conducted
    - test: configuration of the test
    - exception: the exception returned
    - timestamp: the timestamp when the test was performed
    

    
 