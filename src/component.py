import json
import logging
import os
import re
import sys
from datetime import datetime
from pathlib import Path

import pandas as pd
from great_expectations.dataset import SqlAlchemyDataset
from kbc.env_handler import KBCEnvHandler
from snowflake.connector import connect
from snowflake.connector.pandas_tools import write_pandas
from snowflake.sqlalchemy import URL
from sqlalchemy import create_engine

sys.tracebacklimit = 3

# configuration variables
TABLES_DICT = 'tables'
OUTPUT_BUCKET = 'output_bucket'

# #### Keep for debug
KEY_DEBUG = 'debug'

MANDATORY_PARS = [TABLES_DICT, OUTPUT_BUCKET]
MANDATORY_IMAGE_PARS = []

# for easier local project setup
DEFAULT_DATA_DIR = Path(__file__).resolve().parent.parent.joinpath('data').as_posix() \
    if not os.environ.get('KBC_DATADIR') else None

APP_VERSION = 'DEV'


class Component(KBCEnvHandler):

    def __init__(self, debug=False):
        KBCEnvHandler.__init__(self, MANDATORY_PARS,
                               log_level='DEBUG' if debug else 'INFO')  # override debug from config
        if self.cfg_params.get(KEY_DEBUG):
            debug = True
        if debug:
            logging.getLogger().setLevel(logging.DEBUG)
        logging.info('Running version %s', APP_VERSION)
        logging.info('Loading configuration...')
        logging.getLogger('snowflake.connector').setLevel(logging.WARNING)  # avoid detail logs from the library

        try:
            self.validate_config(MANDATORY_PARS)
            self.validate_image_parameters(MANDATORY_IMAGE_PARS)
        except ValueError as e:
            logging.exception(e)
            exit(1)

    def create_manifest(self, table_path, columns, primary_keys, incremental_flag, destination):
        with open(table_path + '.manifest', 'w') as _man_file:
            json.dump(
                {
                    'columns': columns,
                    'incremental': incremental_flag,
                    'primary_key': primary_keys,
                    'destination': destination
                },
                _man_file
            )

    def get_table_columns(self, table_name):
        """
        outputs list of two values - list of columns available for the table and the table name
        """
        with open(f'{self.tables_in_path}/{table_name}.manifest') as manifest:
            man_file = json.load(manifest)
            try:
                return man_file['columns'], table_name
            except KeyError:
                logging.error(f'The manifest for table {table_name} does not contain the key columns!')
                sys.exit(1)
            except Exception as e:
                logging.error('Exception encountered in the get_table_columns function.')
                logging.error(e)
                sys.exit(1)

    def get_table_columns_types(self, table_name):
        """
        outputs a dictionary - dict(column_name:type)
        """
        with open(f'{self.tables_in_path}/{table_name}.manifest') as manifest:
            column_metadata = json.load(manifest)["column_metadata"]
            column_types = {}
            for column_name in column_metadata.keys():
                for metadata_set in column_metadata[column_name]:
                    try:
                        if metadata_set["key"] == "KBC.datatype.basetype":
                            column_types[column_name] = metadata_set["value"]
                    except KeyError:
                        pass
                if column_name in column_types:
                    pass
                else:
                    column_types[column_name] = 'VARCHAR(16777216)'
        return column_types

    def get_tables_coltypes(self):
        """
        outputs a dictionary - table_name:dict(column_name:type)
        """
        input_tables = [re.sub('.manifest$', '', i) for i in os.listdir(self.tables_in_path) if
                        ('.DS_Store' not in i)]
        tables_column_types = {}
        for in_table in input_tables:
            tables_column_types[in_table] = self.get_table_columns_types(in_table)

        return tables_column_types

    def get_required_columns_from_test(self, table_name, test_name, test_specs):
        """
        outputs list of two values - list of columns required for the test and the table name
        arguments:
        table_name --
        test_name --
        test-specs --
        """
        if 'expect_column_pair' in test_name:
            return test_specs.split(',')[:2], table_name

        elif 'expect_multicolumn' in test_name:
            return test_specs.split(',')[0], table_name

        elif 'expect_column' in test_name:
            return [test_specs.split(',')[0].lstrip('\'').rstrip('\'')], table_name

        elif 'expect_table' in test_name:
            return [], table_name

        elif 'expect_file' in test_name:
            logging.error('File tests are not supported.')

    def check_required_vs_available_columns(self):
        """
        returns three dictionaries
        missing_columns - {table:[col_list]}. contains info about columns that are required for the tests but not
                          available
        available_columns - {table:[col_list]}. contains info about columns that are present in the tables
        available_columns - {table:[col_list]}. contains info about columns that are required for the tests
        """
        # get the tables in the input mapping
        input_tables = [re.sub('.manifest$', '', i) for i in os.listdir(self.tables_in_path) if
                        ('.DS_Store' not in i)]

        # get available tables and their columns
        available_tables_cols = {}
        for input_table in input_tables:
            cols_real = self.get_table_columns(table_name=input_table)
            available_tables_cols[cols_real[1]] = cols_real[0]

        # get required tables and their columns
        required_tables_cols = {}
        for tested_table in self.cfg_params['tables']:
            for test in tested_table['tests']:
                cols_required = self.get_required_columns_from_test(table_name=tested_table['table_name'],
                                                                    test_name=test['type'],
                                                                    test_specs=test['test_specification'])
                if tested_table['table_name'] in required_tables_cols.keys():
                    required_tables_cols[tested_table['table_name']].extend(cols_required[0])
                else:
                    required_tables_cols[tested_table['table_name']] = cols_required[0]

        missing_columns = {}
        for table in required_tables_cols.keys():
            missing_cols_for_table = list(set(required_tables_cols[table]) - set(available_tables_cols[table]))
            if len(missing_cols_for_table) > 0:
                missing_columns[table] = missing_cols_for_table

        return missing_columns, available_tables_cols, required_tables_cols

    def build_the_rename_table_query(self, table_name):
        """
        returns a string
        """
        return f'ALTER TABLE IF EXISTS "{table_name}" RENAME TO "{table_name.upper()}"'

    def build_the_rename_column_query(self, table_name, column):
        """
        returns a string
        """
        return f"ALTER TABLE IF EXISTS \"{table_name}\"  RENAME COLUMN  \"{column}\" TO {column.upper()};"

    def build_the_create_or_replace_table_query(self, table_name, column_types):
        """
        returns a string
        """
        column_statements_list = [f'"{k}" :: {column_types[table_name][k]} AS "{k}"' for k in
                                  column_types[table_name].keys()]
        select_statement = ','.join(column_statements_list)
        return f'CREATE OR REPLACE TABLE "{table_name}" AS SELECT {select_statement} FROM "{table_name}"'

    def build_engine(self, creds):
        """
        creates the engine used for connecting to the Snowflake Workspace
        """
        engine = create_engine(URL(
            account=creds["account"],
            user=creds["user"],
            password=creds["password"],
            database=creds["database"],
            schema=creds["schema"],
            warehouse=creds["warehouse"],
            cache_column_metadata=True,
            echo=True
        ))
        return engine

    def execute_query(self, engine, query):
        """
        executes the query in the Snowflake workspace specified by the engine
        """
        try:
            connection = engine.connect()
            results = connection.execute(query).fetchall()
            logging.info(results[0])
        except Exception as e:
            logging.info(repr(e))
        finally:
            connection.close()

    def create_snfk_table_from_pd(self, df, table_name, connection):
        """
        loads a pandas Dataframe to a Snowflake workspace specified by credentials parameters
        arguments:
        creds -- dictionary containing the connection specs
        df --
        table -- the name of the object that will be created
        """

        columns_to_create = [f'\"{c}\" string' for c in list(df.columns)]
        query = ','.join(columns_to_create)

        connection.cursor().execute(f'CREATE OR REPLACE TABLE {table_name}({query})')

        if df.shape[0] > 0:
            success, nchunks, nrows, _ = write_pandas(conn=connection, df=df, table_name=table_name)
            if success:
                logging.info(f'The table {table_name} has been loaded to the snowflake workspace.')
            # connection.close()
            return 0
        else:
            logging.info(f'The table {table_name} has been loaded to the snowflake workspace, it doesn\'t have any '
                         f'rows.')
            # connection.close()
            return 0

    def uppercase_objects_in_snowflake_workspace(self, db_engine, available_tables_cols):
        """
        because GE and SqlAlchemy do not deal well with all lowercase object names, we need to uppercase all
        all-lowercase object names before running the data quality tests

        arguments:
        available_tables_cols -- dictionary containing tables and their columns
        db_engine --
        """
        for table in available_tables_cols.keys():
            if table.lower() == table:
                self.execute_query(engine=db_engine, query=self.build_the_rename_table_query(table_name=table))
                logging.info(f'Object {table}: name has been uppercased')
                result_table_name = table.upper()
            else:
                logging.info(f"Table {table}: table name is not all lowercase, not uppercasing this one.")
                result_table_name = table
            for column in available_tables_cols[table]:
                if column == column.lower():
                    self.execute_query(engine=db_engine,
                                       query=self.build_the_rename_column_query(table_name=result_table_name,
                                                                                column=column))
            logging.info(f'Object {table}: all-lowercase columns have been uppercased')
        logging.info('All tables have been succesfully uppercased.')

        return 0

    def cast_datatypes_objects_in_snowflake_workspace(self, db_engine, tables_column_types):
        """
        performs create or replace table and casts columns with proper datatypes

        arguments:
        tables_column_types -- dictionary containing tables and column types
        db_engine --
        """
        for table in tables_column_types.keys():
            create_or_replace_query = self.build_the_create_or_replace_table_query(table, tables_column_types)
            self.execute_query(engine=db_engine, query=create_or_replace_query)
            logging.info(f'Object {table}: columns have been casted')

        logging.info('Casting finished')

    def run_ge_tests(self, tables, engine, missing_columns):
        """
        this function executes the defined tests and returns two lists:
            - ignored_tests (tests that could not be performed because of some issue and the issue details)
            - performed_tests (test that were performed and their results)
        """
        ignored_tests = []
        performed_tests = []
        for table in tables:
            table_name = table['table_name']
            table_missing_columns = missing_columns.get(table_name)
            dataset = SqlAlchemyDataset(table_name=table_name, engine=engine)  # noqa
            for test in table['tests']:
                t_type = test['type']
                test_specification = test['test_specification']
                test_column = test_specification.split(',', 1)[0].replace("'", "").strip()
                if table_missing_columns is not None and test_column in table_missing_columns:
                    logging.info(table_name, test_column, 'missing')
                    ignored_tests.append([str(table_name), str(test_column), 'missing_column'])
                else:
                    test_result_format = test['result_format']
                    ge_test = f'dataset.{t_type}({test_specification}, result_format="{test_result_format}")'
                    try:
                        expectation_result = eval(ge_test)
                        logging.info(expectation_result)
                        performed_tests.append([table_name,
                                                str(expectation_result.expectation_config[
                                                        'expectation_type']),
                                                str(expectation_result.success),
                                                str(expectation_result.expectation_config['kwargs']),
                                                str(test_result_format),
                                                str(expectation_result.result),
                                                str(datetime.now().strftime("%Y-%m-%d %H:%M:%S"))])
                    except Exception as e:
                        if 'snowflake.connector.errors.ProgrammingError' in e.args[0]:
                            logging.info('The query sent is invalid!')
                            logging.info(e)
                        elif e.args[0] == 'invalid syntax':
                            logging.info(f'The syntax of the GE function is invalid. Please check how you specified '
                                         f'the tests for table {table_name}!')
                            logging.info(f'The problematic test is: {e.args[1][3]}.')

                        ignored_tests.append([str(table_name), str(ge_test), str(e), str(datetime.now().strftime(
                            "%Y-%m-%d %H:%M:%S"))])
                        pass
        return ignored_tests, performed_tests

    def run(self):
        """
        Main execution code
        """
        snfk_authorisation = self.configuration.get_authorization()['workspace']
        params = self.cfg_params  # noqa
        credentials = {
            "account": snfk_authorisation['host'].replace('.snowflakecomputing.com', ''),
            "user": snfk_authorisation['user'],
            "password": snfk_authorisation['password'],
            "database": snfk_authorisation['database'],
            "schema": snfk_authorisation['schema'],
            "warehouse": snfk_authorisation['warehouse']
        }

        missing_columns, available_tables_cols, required_tables_cols = self.check_required_vs_available_columns()
        tables_n_column_types = self.get_tables_coltypes()

        db_engine = self.build_engine(creds=credentials)
        db_connection = connect(account=credentials["account"],
                                user=credentials["user"],
                                password=credentials["password"],
                                database=credentials["database"],
                                schema=credentials["schema"],
                                warehouse=credentials["warehouse"])

        # cast the columns as the datatypes specified in manifest
        self.cast_datatypes_objects_in_snowflake_workspace(db_engine=db_engine,
                                                           tables_column_types=tables_n_column_types)

        # make sure the Great Expectations library can deal with the tables
        self.uppercase_objects_in_snowflake_workspace(db_engine=db_engine,
                                                      available_tables_cols=available_tables_cols)

        # executes the actual tests
        ignored_tests, performed_tests = self.run_ge_tests(tables=self.cfg_params['tables'],
                                                           engine=db_engine,
                                                           missing_columns=missing_columns)

        # writes the failed tests table to Snowflake
        ignored_tests_headers = ['table_name', 'test', 'exception', 'timestamp']
        ignored_tests_df = pd.DataFrame(ignored_tests, columns=ignored_tests_headers)
        self.create_snfk_table_from_pd(df=ignored_tests_df,
                                       table_name='IGNORED_TESTS', connection=db_connection)

        self.create_manifest(table_path=f"{self.tables_out_path}/IGNORED_TESTS", columns=ignored_tests_headers,
                             primary_keys=ignored_tests_headers, incremental_flag=True,
                             destination=f"in.c-{self.cfg_params['output_bucket']}.IGNORED_TESTS")

        # writes the performed tests table to Snowflake
        performed_tests_headers = ['table_name', 'test', 'success', 'config', 'log_level', 'result', 'timestamp']
        performed_tests_df = pd.DataFrame(performed_tests, columns=performed_tests_headers)
        self.create_snfk_table_from_pd(df=performed_tests_df,
                                       table_name='PERFORMED_TESTS', connection=db_connection)

        db_connection.close()
        db_engine.dispose()

        self.create_manifest(table_path=f"{self.tables_out_path}/PERFORMED_TESTS",
                             columns=performed_tests_headers,
                             primary_keys=performed_tests_headers, incremental_flag=True,
                             destination=f"in.c-{self.cfg_params['output_bucket']}.PERFORMED_TESTS")

        logging.info('success')


"""
        Main entrypoint
"""
if __name__ == "__main__":
    if len(sys.argv) > 1:
        debug_arg = sys.argv[1]
    else:
        debug_arg = False
    try:
        comp = Component(debug_arg)
        comp.run()
        exit(0)
    except Exception as exc:
        logging.exception(exc)
        exit(1)
