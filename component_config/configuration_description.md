DQ test app

- **Table Input Mapping**:
    Specify the tables you want to have checked. Make sure all necessary columns are there.
    
- **Table Output Mapping**:
    The tables in the Snowflake workspace are called FAILED_TESTS and PERFORMED_TESTS. Make sure you are 
    fetching those
 
- **Add Table**:
    Specify the tables you want to perform the tests on. Make sure the Table Name conforms to the name
    in the input mapping
- **Add Test**:
    Specify the tests you want to perform on the table
    - **Test Type**: Choose the test you want to perform
    - **Log Level**: Specify how detailed logs you want to get back. You can use `BOOLEAN_ONLY`, `BASIC`, `SUMMARY`,
    or `COMPLETE`. Details on the log levels [here](https://docs.greatexpectations.io/en/v0.4.0/result_format.html) (look 
    for parameter result_format)
    - **Test Specification**: The parameters passed to the function. You can find the specific parameters that need
    to be passed to each expectation [here](https://docs.greatexpectations.io/en/v0.4.0/glossary.html). Please make
    sure the column names are exactly the same as the column names in the table.
- **Output Bucket**:
    - The name of the output bucket. If you use value `sample`, the output bucket will be `in.c-sample`.